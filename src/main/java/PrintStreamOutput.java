import java.io.IOException;
import java.io.OutputStream;

public class PrintStreamOutput implements IOutput{

    private final OutputStream output;

    public PrintStreamOutput(OutputStream stream) {
        output = stream;
    }

    public void print(MessageData message) {
        writeToOutput(message.authorUsername + " : " + message.content + '\n');
    }

    public void print(String text) {
        writeToOutput(text + '\n');
    }

    private void writeToOutput(String line) {
        try {
            output.write(line.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
