
public interface IOutput {
    void print(MessageData message);
    void print(String text);
}
