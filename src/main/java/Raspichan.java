import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.EventDispatcher;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;

public class Raspichan {

    private static GatewayDiscordClient client;
    private static String ownerId = null;
    private static final IOutput output = new PrintStreamOutput(System.out);

    public static void main(String[] args) {
        if(args.length >= 2) {
            createClient(args[0]);
            ownerId = args[1];
            runBot();
        }
        else
            output.print("No Discord Token was provided.");
    }

    public static void createClient(String token) {
        client = DiscordClientBuilder.create(token)
            .build()
            .login()
            .block();
    }

    public static void runBot() {
        if(client != null) {
            subscribeToEvents(client);
            client.onDisconnect().block();
        }
    }

    public static void subscribeToEvents(GatewayDiscordClient client) {
        try{
            EventDispatcher dispatcher = client.getEventDispatcher();
            subscribeToReadyEvent(dispatcher);
            subscribeToMessages(dispatcher);
        } catch(NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void subscribeToReadyEvent(EventDispatcher dispatcher) {
        dispatcher.on(ReadyEvent.class).subscribe(event -> {
            User self = event.getSelf();
            output.print(String.format("Logged in as %s#%s", self.getUsername(), self.getDiscriminator()));
        });
    }

    public static void subscribeToMessages(EventDispatcher dispatcher) {
        dispatcher.on(MessageCreateEvent.class)
                .subscribe(event -> process(event.getMessage()));
    }

    public static void process(Message message) {
        MessageData msg = new MessageData(message);
        if(msg.isDisconnectCommand())
            client.logout().block();
        else
            output.print(msg);
    }

    public static String getOwnerId() {
        return ownerId;
    }
}
