import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;

import java.util.Optional;

public class MessageData {

    public String authorUsername = "None";
    public String authorId = "None";
    public String content;

    public MessageData(Message message) {
        Optional<User> author = message.getAuthor();
        if(author.isPresent()) {
            User user = author.get();
            authorUsername = user.getUsername();
            authorId = user.getId().asString();
        }
        content = message.getContent();
    }

    public boolean isDisconnectCommand() {
        return authorId.equals(Raspichan.getOwnerId()) && content.equals("!stop");
    }
}
